from django.db.models import fields
from django.forms.models import ModelForm
from detailBarang.models import Barang
from django import forms

class BarangForm(ModelForm):
    class Meta:
        model = Barang
        fields = ['namaBarang', 'jumlahStok', 'hargaBarang', 'deskripsiBarang']

        widgets = {
            'namaBarang' : forms.TextInput(attrs={'class' : 'form-control'}),
            'jumlahStok' : forms.TextInput(attrs={'class' : 'form-control'}),
            'hargaBarang' : forms.TextInput(attrs={'class' : 'form-control'}),
            'deskripsiBarang' : forms.Textarea(attrs={'class' : 'form-control'}),
        }