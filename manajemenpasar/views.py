from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.contrib.auth.models import User 
from detailBarang.models import Barang
from .forms import BarangForm

# Create your views here.
def index(request):
    semuabarang = Barang.objects.all().order_by('-id')

    context = {
        'semuabarang':semuabarang,
    }

    return render(request, 'index.html', context)

def createbarang(request):
    if request.method == 'POST':
        form = BarangForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/manajemenpasar/')
    else:
        form = BarangForm()
    
    context = {
        'form': form
        }

    return render(request, 'createbarang.html', context)

def detailbarang(request, detail_id):
	detailBarang = Barang.objects.get(id=detail_id)
	harga = str(detailBarang.hargaBarang)[::-1]
	hargacopy = ""
	length = len(harga)
	count = len(harga)

	while count > 3:
		hargacopy += harga[length - count:length - count + 3] + "."
		count -= 3

	hargacopy += harga[length - count:length]
	harga = hargacopy[::-1]
	
	context = {
		'id':detailBarang.id,
		'namaBarang':detailBarang.namaBarang, 
		'jumlahStok':detailBarang.jumlahStok,
		'hargaBarang':harga,
		'deskripsiBarang':detailBarang.deskripsiBarang,
    }
	
	return render(request, 'detailbarang.html', context)

def deletebarang(request, delete_id):
    Barang.objects.filter(id=delete_id).delete()
    return redirect('/manajemenpasar/')

def updatebarang(request, update_id):
    instance = get_object_or_404(Barang, id=update_id)
    form = BarangForm(request.POST or None, instance=instance)

    if form.is_valid():
        form.save()
        return redirect('/manajemenpasar/')
    
    context = {
        'form': form
        }

    return render(request, 'createbarang.html', context)