from django.urls import path
from . import views

app_name='manajemenpasar'

urlpatterns = [
    path('manajemenpasar/', views.index, name='index'),
    path('manajemenpasar/tambahbarang/', views.createbarang, name='createbarang'),
    path('manajemenpasar/<int:detail_id>/', views.detailbarang, name='detailbarang'),
    path('manajemenpasar/deletebarang/<int:delete_id>/', views.deletebarang, name='deletebarang'),
    path('manajemenpasar/updatebarang/<int:update_id>/', views.updatebarang, name='updatebarang'),
]
