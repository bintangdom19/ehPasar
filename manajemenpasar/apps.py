from django.apps import AppConfig


class ManajemenpasarConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'manajemenpasar'
