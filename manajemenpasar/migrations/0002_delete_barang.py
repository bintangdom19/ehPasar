from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('manajemenpasar', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='Barang',
        ),
    ]
