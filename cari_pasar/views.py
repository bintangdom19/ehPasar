from django.shortcuts import render,redirect

# Create your views here.
def cari_pasar(request):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		return render(request, 'cari_pasar.html')
