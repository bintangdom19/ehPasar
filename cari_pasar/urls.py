from django.urls import path
from . import views

app_name='cari_pasar'

urlpatterns = [
    path('cari_pasar', views.cari_pasar, name='cari_pasar'),
]