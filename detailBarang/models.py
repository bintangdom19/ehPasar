from django.db import models

# Create your models here.
class Barang(models.Model):
    id = models.AutoField(primary_key=True)
    namaBarang = models.CharField(max_length=100)
    jumlahStok = models.PositiveIntegerField()
    hargaBarang = models.PositiveIntegerField()
    deskripsiBarang = models.CharField(max_length=300)
    def __str__(self):
        return self.namaBarang
