from django.urls import re_path, path
from .views import detailBarang

urlpatterns = [
    path('catalog/detailBarang/<int:barang_id>/', detailBarang, name='barang'),

]
