from django.urls import path
from . import views

app_name = "feedback"

urlpatterns = [
    path('feedback', views.feedback, name='feedback'),
    path('savefeedback', views.savefeedback, name='savefeedback'),
]