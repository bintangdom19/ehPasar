from django.shortcuts import render
from detailBarang.models import Barang
import wishlist
from wishlist.models import Wishlist
from django.shortcuts import render,redirect
from django.core import serializers
from django.http import HttpResponse,JsonResponse
import json

# Create your views here.
def listBarang(request):
    if 'nama' not in request.session:
        return redirect('main:home')
    user_type = request.session['role']
    
    if user_type == "pembeli" :
        filtering = Barang.objects.all()
        wishlist = Wishlist.objects.all()
        context ={
            'post' : filtering,
            'wishlist' : wishlist
        }
        return render(request,'listBarang/listBarang.html', context)