from django import forms
from .models import Wishlist

class AddWishlistForm(forms.ModelForm):
    class Meta:
        model = Wishlist
        exclude = ('barang','')