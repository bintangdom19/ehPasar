from django.shortcuts import render, redirect
from .forms import *
from detailBarang.models import Barang
from .models import Wishlist

# Create your views here.
def getWishlist(request):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		wishlist = Wishlist.objects.all()
		context ={
			'wishlist' : wishlist
		}
		return render(request, 'wishlist.html',context)

def insertBarangWishlist(request, barang_id):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		if (request.method == 'POST'):
			form = AddWishlistForm(request.POST)
			if (form.is_valid()):
				barang = Barang.objects.filter(id=barang_id)[0]
				wishlist = form.save()
				wishlist.barang = barang
				wishlist.save()
				return redirect('listBarang:listBarang')

def detail_wishlist(request, barang_id):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		post = Wishlist.objects.filter(id=barang_id)[0]
		response = {'barang' : post}
		return render(request, 'detailWishlist.html', response)

def removeBarangWishlist(request, barang_id):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		if (request.method == 'POST'):
			barang = Wishlist.objects.filter(id=barang_id)
			print(barang)
			barang.delete()
			print("yes")
		return redirect('wishlist:wishlist')

