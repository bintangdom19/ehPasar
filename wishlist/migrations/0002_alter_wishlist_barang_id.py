# Generated by Django 3.2.6 on 2021-12-07 09:04

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('detailBarang', '0001_initial'),
        ('wishlist', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='wishlist',
            name='barang_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='detailBarang.barang'),
        ),
    ]
