from django.db import models
from django.db.models.fields import IntegerField
from detailBarang.models import Barang
# Create your models here.

class Wishlist(models.Model):
    barang_id = models.ForeignKey(Barang, on_delete=models.CASCADE)