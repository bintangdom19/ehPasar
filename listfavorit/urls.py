from django.urls import path
from . import views

app_name='listfavorit'

urlpatterns = [
    path('listfavorit/', views.listfavorit, name='listfavorit'),
    path('listfavorit/<int:idBarang>', views.detailListFavorit, name='detailListFavorit'),
    path('listfavorit/add/<int:idBarang>/', views.addListFavorit, name='addListFavorit'),
    path('listfavorit/delete/<int:idBarang>/', views.deleteListFavorit, name='deleteListFavorit'),
    path('checkout/', views.checkout, name='checkout'),
]