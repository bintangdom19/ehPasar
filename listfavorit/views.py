import re
from django.http import response
from django.shortcuts import render, redirect
from .models import Listfavorit
from .forms import ListfavoritForm
from detailBarang.models import Barang

# Create your views here.

def listfavorit(request):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		listfavorit = Listfavorit.objects.all()
		response ={
			'listfavorit' : listfavorit
		}
		return render(request, 'listfavorit.html',response)

def addListFavorit(request, idBarang):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		listfavorit = Listfavorit.objects.filter(id=idBarang)
		if (request.method == 'POST' and listfavorit == None):
			form = ListfavoritForm(request.POST or None)
			if (form.is_valid()):
				barang = Barang.objects.filter(id=idBarang)[0]
				listfavorit = form.save()
				listfavorit.barang = barang
				listfavorit.save()
				return redirect(request, 'listfavorit.html')

		form = ListfavoritForm(request.POST or None)

def detailListFavorit(request, idBarang):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		barang = Listfavorit.objects.get(id=idBarang)
		response={
			'barang' : barang,
		}
		return render(request, 'detaillistfavorit.html',response)

def deleteListFavorit(request, idBarang):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		if (request.method == 'POST'):
			barang = Listfavorit.objects.filter(id=idBarang).delete()
		return render(request, 'listfavorit.html')

def checkout(request,):
	if 'nama' not in request.session:
		return redirect('main:home')
	user_type = request.session['role']
	
	if user_type == "pembeli" :
		return render(request, 'checkout.html')