from django.shortcuts import render,redirect
from django.contrib.auth import login, authenticate, logout
from .models import User
from .forms import *

# Create your views here.
def daftar(request):
	return render(request, 'signup.html')

def signup(request):
	if (request.method == 'POST'):
		form = SignUpForm(request.POST)
		print(form)
		if (form.is_valid()):
			user = form.save()
			raw_password = form.cleaned_data.get('password1')
			user = authenticate(username=user.nama, password=raw_password)
			login(request, user)
			print('yes')
			request.session['nama'] = user.nama
			request.session['role'] = user.role
			return redirect('cari_pasar:cari_pasar')
	return render(request, 'signup.html')
def coba(request):
	print(request.session['nama'])
	return render(request, 'coba.html')
def signin(request):
	response = {}
	if (request.method == 'POST'):
		username = request.POST.get('nama')
		password = request.POST.get('password')
		user = authenticate(nama=username, password=password)
		if user is not None:
			login(request, user)
			request.session['nama'] = user.nama
			request.session['role'] = user.role
			return redirect('cari_pasar:cari_pasar')
		else:
			response['message'] = 'Username / Password salah'
	return render(request, 'signin.html')


def signOut(request):
    logout(request)
    return redirect('main:home')

